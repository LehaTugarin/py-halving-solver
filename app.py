# theory ref http://pycode.ru/files/hw1.pdf 

def func(x):
    return float(x**5+12*x-75)

epsilon = 1*10**(-8)
a = float(input())
b = float(input())

while True:
    
    x = (a+b)/2
    print("x={}".format(x))
    if abs(func(x))<= epsilon:
        print("Solve = {}".format(x))
        break

    f_a = func(a)
    f_x = func(x)

    if float(f_a*f_x)<0:
        b=x
    else:
        a = x
